package dicegames;

import java.util.Scanner;

public class PigGame {
	// Players & Scores
	private int player1Score = 0;
	private int player2Score = 0;
	private String player1;
	private String player2;
	private int player1TurnScore = 0;
	private int player2TurnScore = 0;
	private boolean whosTurn = false;
	// Win point count
	private int pointsToWin;
	// Dice
	private Die die;
	// Scanner for reading user input
	private Scanner scan;

	/**
	 * Constructor for objects of class PigGame
	 */
	public PigGame() {
		die = new Die();
		scan = new Scanner(System.in);
	}

	/**
	 * welcomeToPigs, lets gather the player names and winning points!.
	 */
	public void welcomeToPigs() {
		System.out.println("Velkommen til Pigs!");
		System.out.println("Før vi begynder skal du sætte spillernavne.");
		System.out.println("Indtast navnet på spiller 1: ");
		player1 = scan.nextLine();
		System.out.print("Indtast navnet på spiller 2: ");
		player2 = scan.nextLine();
		System.out.println(player1 + " mod " + player2);
		System.out.println("Hvor mange points vil i/du spille til?");

		pointsToWin = scan.nextInt();
		System.out.println("Antal points for a vinde: " + pointsToWin);
		System.out.println("Spillet Begynder!");

		playerTurn();

	}

	/**
	 * updateScore, used to update the score of each player with the amount of
	 * total hits in a round + adds 0 as points if the player hits 1.
	 */

	public void updateScore() {
		if (!whosTurn && die.getFaceValue() == 1) {
			player1TurnScore = 0;
			player1Score = player1Score + player1TurnScore;
			System.out.println(player1 + " Slog 1, du får 0 points");
			whosTurn = true;
			playerTurn();
		} else if (!whosTurn && die.getFaceValue() != 1) {
			player1TurnScore = player1TurnScore + die.getFaceValue();
			System.out.println(player1 + " Slog " + die.getFaceValue() + ", du får " + die.getFaceValue()
					+ " points. TotalTur: " + player1TurnScore + " Gemt: " + player1Score + " Total: "
					+ (player1Score + player1TurnScore));
			if (player1Score + player1TurnScore >= pointsToWin) {
				System.out.println(player1 + " Vinder spillet!");
			} else {
				playerTurn();
			}
		} else if (whosTurn && die.getFaceValue() == 1) {
			player2TurnScore = 0;
			player2Score = player2Score + player2TurnScore;
			System.out.println(player2 + " Slog 1, du får 0 points");
			whosTurn = false;
			playerTurn();
		} else if (whosTurn && die.getFaceValue() != 1) {
			player2TurnScore = player2TurnScore + die.getFaceValue();
			System.out.println(player2 + " Slog " + die.getFaceValue() + ", du får " + die.getFaceValue()
					+ " points. TotalTur: " + player2TurnScore + " Gemt: " + player2Score + " Total: "
					+ (player2Score + player2TurnScore));
			if (player2Score + player2TurnScore >= pointsToWin) {
				System.out.println(player2 + " Vinder spillet!");
			} else {
				playerTurn();
			}
		}
	}

	/**
	 * playerTurn checks whos turn it is + collects input if you wants to throw
	 * or stop the round.
	 */

	public void playerTurn() {
		if (!whosTurn) {
			System.out.println(player1 + " Det er din tur. vælg funktion 'Kast' 'Stop'");
		} else if (whosTurn) {
			System.out.println(player2 + " Det er din tur. vælg funktion 'Kast' 'Stop'");
		}
		String kaststop = scan.next();
		if (kaststop.equalsIgnoreCase("Kast")) {
			die.roll();
			updateScore();

		} else if (kaststop.equalsIgnoreCase("Stop")) {
			if (!whosTurn) {
				player1Score = player1Score + player1TurnScore;
				player1TurnScore = 0;
				whosTurn = true;
			} else if (whosTurn) {
				player2Score = player2Score + player2TurnScore;
				player2TurnScore = 0;
				whosTurn = false;
			}
			updateScore();

		} else if (!kaststop.equalsIgnoreCase("kast") || !kaststop.equalsIgnoreCase("Stop")) {
			System.out.println("Du skal vælge om du vil kaste eller stoppe. brug 'Kast' eller 'Stop'");
			playerTurn();
		}
	}

	/**
	 * startGame, runs welcomeToPigs, just to make it easy to start the game in
	 * the PlayPigs.java file.
	 */
	public void startGame() {
		welcomeToPigs();

	}
}
