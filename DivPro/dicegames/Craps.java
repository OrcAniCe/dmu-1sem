package dicegames;

public class Craps {

	private Die die1;
	private Die die2;

	private boolean gamewon;
	private int totalRolls = 0;

	/**
	 * Constructor for Craps.
	 */
	public Craps() {
		die1 = new Die();
		die2 = new Die();
	}

	/**
	 * Sum of dices
	 */
	public int sumOfDices() {
		int diceSum = die1.getFaceValue() + die2.getFaceValue();
		return diceSum;
	}

	/**
	 * Welcome to game
	 */
	public void welcomeToCraps() {
		System.out.println("Velkommen til spillet Craps!");
	}

	/**
	 * GameOver!
	 */
	public void gameOver() {
		if (gamewon) {
			System.out.println(
					"Tillykke, du slog " + sumOfDices() + " Du kastede " + totalRolls + " gange før du vandt!");
		} else if (!gamewon) {
			System.out.println(
					"Desværre, du slog " + sumOfDices() + " Du kastede " + totalRolls + " gange før du tabte!");
		}

	}

	public void takeTurn() {
		// Terningerne kastes
		die1.roll();
		die2.roll();
		totalRolls++;
		// Tjekker om første kast er vundet eller tabt. Ellers kaster vi igen!

		if (sumOfDices() == 2 || sumOfDices() == 3 || sumOfDices() == 12) {
			gamewon = false;
		} else if (sumOfDices() == 7 || sumOfDices() == 11) {
			gamewon = true;
		} else {
			// gemmer det første kast, hvis det ikke er 2,3,7,11 eller 12.
			int savedRoll = sumOfDices();

			boolean keepRunning = true;
			System.out.println("Du slog: " + savedRoll + ", kan du slå det samme tal igen?");
			die1.roll();
			die2.roll();
			totalRolls++;
			System.out.println("Du slog: " + sumOfDices());
			// While som bliver ved med at rulle til vi rammer vores tal,
			// eller indtil at vi rammer 7
			while (sumOfDices() != 7 && keepRunning) {
				if (sumOfDices() == savedRoll) {
					gamewon = true;
					keepRunning = false;
				} else {
					die1.roll();
					die2.roll();
					System.out.println("Du slog: " + sumOfDices());
					totalRolls++;
				}
				if (sumOfDices() == 7) {
					gamewon = false;
					keepRunning = false;
				}
			}
		}
		gameOver();
	}

	/**
	 * startGame so we easyly can make a class for running the game.
	 */

	public void startGame() {
		welcomeToCraps();
		takeTurn();
	}

}
