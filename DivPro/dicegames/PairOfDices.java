package dicegames;

import java.util.Scanner;

/**
 * This class models one pair of dices. This is useful for games where you have
 * to throw two dice at once.
 */
public class PairOfDices {

	/**
	 * How many games has been played + dice count + equal count + highest roll.
	 */
	private int gameAmount = 0;
	private int equalRolls = 0;
	private int highestRoll = 0;
	private int Ones = 0;
	private int Twos = 0;
	private int Threes = 0;
	private int Fours = 0;
	private int Fives = 0;
	private int Sixs = 0;

	/**
	 * The first die in the pair.
	 */
	private Die die1;
	/**
	 * The second die in the pair.
	 */
	private Die die2;

	/**
	 * Scanner to read user input
	 */
	private Scanner scan;

	/**
	 * Constructor for objects of class PairOfDices
	 */
	public PairOfDices() {
		die1 = new Die();
		die2 = new Die();
		scan = new Scanner(System.in);
	}

	/**
	 * rollBothDices
	 */
	private void rollBothDices() {
		die1.roll();
		die2.roll();
		int roll1 = die1.getFaceValue();
		int roll2 = die2.getFaceValue();
		gameAmount++;
		checkDiceEyes(die1);
		checkDiceEyes(die2);
		if (die1.getFaceValue() == die2.getFaceValue()) {
			equalRolls++;
		}
		if (sumOfDices() > highestRoll) {
			highestRoll = sumOfDices();
		}
		System.out.println("Du har kastet: " + roll1 + " " + roll2 + " Total: " + sumOfDices());
	}

	/**
	 * sumOfDices
	 */
	public int sumOfDices() {
		int diceSum = die1.getFaceValue() + die2.getFaceValue();
		return diceSum;
	}

	/**
	 * Setter and Getter for highestRoll
	 */

	public int getHighestRoll() {
		return highestRoll;
	}

	public void setHighestRoll(int highestRoll) {
		this.highestRoll = highestRoll;
	}

	/**
	 * Reset Game stats
	 */
	public void resetPairOfDices() {
		gameAmount = 0;
		equalRolls = 0;
		highestRoll = 0;
		Ones = 0;
		Twos = 0;
		Threes = 0;
		Fours = 0;
		Fives = 0;
		Sixs = 0;

	}

	/**
	 * Check dice hit amount, so we can add it to the amount of diceEyes rolled.
	 */
	public void checkDiceEyes(Die die) {
		if (die.getFaceValue() == 1) {
			Ones++;
		} else if (die.getFaceValue() == 2) {
			Twos++;
		} else if (die.getFaceValue() == 3) {
			Threes++;
		} else if (die.getFaceValue() == 4) {
			Fours++;
		} else if (die.getFaceValue() == 5) {
			Fives++;
		} else if (die.getFaceValue() == 6) {
			Sixs++;
		}
	}

	/**
	 * Print game stats - Prints stats after ended game.
	 */
	public void printGameStats() {
		System.out.println("Tak fordi du spillede!");
		System.out.println("Games Played: " + gameAmount);
		System.out.println("Highest Roll: " + highestRoll);
		System.out.println("Equal Hits: " + equalRolls);
		System.out.println("Ones Rolled: " + Ones);
		System.out.println("Twos Rolled: " + Twos);
		System.out.println("Threes Rolled: " + Threes);
		System.out.println("Fours Rolled: " + Fours);
		System.out.println("Fives Rolled: " + Fives);
		System.out.println("Sixs rolled: " + Sixs);
		scan.close();
		resetPairOfDices();
	}

	/**
	 * Welcome to the game ^
	 */
	public void welcomeToPairOfDices() {
		System.out.println("Velkommen til spillet Pair Of Dices");
	}

	/**
	 * Lets Start the game (Enjoy)
	 */
	public void startGame() {
		welcomeToPairOfDices();

		boolean finished = false;

		while (!finished) {
			System.out.println("Vil du kaste terningerne? Angiv Ja eller Nej: ");
			String proceedWithGame = scan.nextLine();
			if (proceedWithGame.equalsIgnoreCase("Nej")) {
				finished = true;
			} else {
				rollBothDices();
			}
		}

		printGameStats();
	}
}
